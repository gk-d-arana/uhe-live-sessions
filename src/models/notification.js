const { DataTypes, DATEONLY } = require('sequelize');
const { sequelize } = require('../db');

const notification = sequelize.define('notification', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  value:{
    type: DataTypes.STRING,
  },
  user: {
    type: DataTypes.INTEGER
  },
  author: {
    type: DataTypes.INTEGER
  },
  isRead: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  type:{
    type: DataTypes.STRING
  }
});


sequelize.sync().then(() => {
  console.log('created successfully!');
}).catch((error) => {
  console.error('Unable to create table : ', error);
});

module.exports={
    notification
}