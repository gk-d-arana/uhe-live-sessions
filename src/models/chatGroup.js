const { DataTypes } = require('sequelize');
const { sequelize } = require('../db');

const ChatGroup = sequelize.define('ChatGroup', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  users: {
    type: DataTypes.STRING
  },
  messages: {
    type: DataTypes.STRING,
  },
  name: {
    type: DataTypes.STRING,
    defaultValue: "chat"
  }
});


sequelize.sync().then(() => {
  console.log('created successfully!');
}).catch((error) => {
  console.error('Unable to create table : ', error);
});

module.exports={
    ChatGroup
}

// SELECT * FROM tableName WHERE columnName LIKE "%#%" OR columnName LIKE "%$%" OR (etc.)