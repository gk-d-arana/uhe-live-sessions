const { Sequelize, DataTypes } = require('sequelize');
const { sequelize } = require('../db');

const chatMessage = sequelize.define('chatMessage', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  value:{
    type: DataTypes.STRING,
  },
  user: {
    type: DataTypes.INTEGER
  },
});


sequelize.sync().then(() => {
  console.log('created successfully!');
}).catch((error) => {
  console.error('Unable to create table : ', error);
});

module.exports={
    chatMessage
}