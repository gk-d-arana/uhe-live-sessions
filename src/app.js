// // https://stackoverflow.com/questions/13143945/dynamic-namespaces-socket-io

// // eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImEyQGEuY29tIiwicm9sZSI6InN0dWRlbnQiLCJzZWNyZXRDb2RlIjoiVTJGc2RHVmtYMS9KQVFFWGtVcVNIYTlZUTFyZ28wTUNUUXh4dmJIV1A2TTVlNXI1OC93aW4vUHc1dkliYVlvZSIsImlhdCI6MTY2MTM4NzgwM30.0UE9dItAa7cZasXmEjopKNnh2LclXIDQYYYTFjl7y7Y

const { startDb, con } = require('./db');
const { decode } = require('jsonwebtoken');
const http = require('http');
const path = require('path');
const express = require('express');
const socketio = require('socket.io');
const cors = require('cors');
const { initChat, sendChat } = require('./api/chat');
const {
  handleNotificationInit,
  markNotificationAsRead,
} = require('./api/notification');
const app = express();
app.use(cors({origin: '*', allowedHeaders: '*'}))
const server = http.createServer(app);
const io = socketio(server);
global.sockets = [];
startDb(io)
  .then(() => {
    function getConnectedSockets() {
      return Object.values(io.of("/").connected);
  }
  
  // getConnectedSockets().forEach(function(s) {
  //     s.disconnect(true);
  // });
    console.log('started db');
  })
  .catch((err) => console.log(err));
//MiddleWares
app.use(express.json());



app.use(
  express.urlencoded({
    extended: false,
  }),
);
app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/public'));
});

app.put('/notifications/:id', (req, res) => {
  const token = decode(req.headers.authorization);
  if (token)
    con.query(
      `SELECT * FROM User WHERE email LIKE '${token.email}';`,
      (err, result) => {
        if (err) console.log(err);
        if (result[0]) {
          markNotificationAsRead(req.params.id, res, con, result[0].id);
        }
      },
    );
  else {
    res.sendStatus(403);
  }
});

io.on('connect', (socket) => {
  console.log(socket.id);
  const query = socket.handshake['query'];
  if (query['token']) {
    const token = decode(query['token']);
    con.query(
      `SELECT * FROM User WHERE email LIKE '${token.email}';`,
      async (err, res) => {
        if (err) socket.disconnect();
        console.log(res[0]);
        const sessionType = query['sessionType'];
        const user = res[0];
        switch (sessionType) {
          case 'vrc_chat':
            const chatGroup = query['chatGroup'];
            socket.userId = user.id;
            global.sockets.push(socket);
            await initChat(con, user, chatGroup, socket);
            socket.on('sendChatMessage', (chatMessage) => {
              sendChat(
                con,
                user,
                chatGroup,
                socket,
                chatMessage,
                io,
              );
            });
          break;
            
          case 'chat':
            const userChatMembers = query['userChatMembers'];
            const chatRoomId = query['chatRoomId'];
            socket.userId = user.id;
            global.sockets.push(socket);
            initChat(con, user, userChatMembers, chatRoomId, socket);
            socket.on('sendChatMessage', (chatMessage) => {
              sendChat(
                con,
                user,
                userChatMembers,
                chatRoomId,
                socket,
                chatMessage,
                io,
              );
            });
            break;
          case 'notification':
            socket.join(`notify${user.id}`);

            handleNotificationInit(con, socket, user).then(() =>
              console.log('attached listener'),
            );
            break;
          default:
            break;
        }

        // socket.emit('init', {data: })
      },
    );
  } else {
    console.log('no token');
    socket.disconnect();
  }

  socket.on('disconnect', function () {
    // socket.leave(room);
    console.log('user disconnected');
  });

  socket.on('chat message', function (msg) {
    io.to(room).emit('chat message', msg);
  });
});

module.exports = server;
