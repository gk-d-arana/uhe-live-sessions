var mysql = require('mysql');

var con = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'e_learning',
  port: '3306',
});

const startDb = async (io) => {
  con.connect(function (err) {
    if (err) throw err;
  });
  const MySQLEvents = require('@rodrigogs/mysql-events');
  const instance = new MySQLEvents(con, {
    startAtEnd: true,
    excludedSchemas: {
      mysql: true,
    },
  });

  await instance.start();

  instance.addTrigger({
    name: 'e_learning',
    expression: '*',
    statement: MySQLEvents.STATEMENTS.ALL,
    onEvent: (event) => {
      // You will receive the events here
      switch (event.type) {
        case 'INSERT':
          switch (event.table) {
            case 'notifications':
              const socket = Array.from(
                io.sockets.adapter.rooms.get(
                  `notify${event.affectedRows[0].after.user}`,
                ),
              )[0];
              con.query(
                `SELECT * FROM chatgroups WHERE id LIKE '${event.affectedRows[0].after.author}';`, (_err,chatGroup)=>{
                  if (chatGroup[0]) {
                    event.affectedRows[0].after.author = {
                      id: chatGroup[0].id,
                      name: chatGroup[0].name,
                    };
                    io.sockets.sockets.get(socket).emit('receiveNotification', event.affectedRows[0].after);
                  }
                }
              );

              break;

            default:
              break;
          }
          // console.log(io.sockets.clients(`notify${event.affectedRows[0].after.user.id}`))
          break;

        default:
          break;
      }
    },
  });
  instance.on(MySQLEvents.EVENTS.CONNECTION_ERROR, (err) =>
    console.log('Connection error', err),
  );
  instance.on(MySQLEvents.EVENTS.ZONGJI_ERROR, (err) =>
    console.log('ZongJi error', err),
  );
};

module.exports = {
  con,
  startDb,
};
