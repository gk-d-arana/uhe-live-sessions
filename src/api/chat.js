const initChat = async (con, user, chatGroup, socket) => {
  let selectQuery = `SELECT * FROM chatgroups WHERE id LIKE '${chatGroup}'`;
  con.query(selectQuery, (error, result) => {
    if (error) console.log(error);


    if (result[0]) {
      
    if (JSON.parse(result[0].users).findIndex((el) => el == user.id) == -1) {
      socket.disconnect();
    }
      for (let i = 0; i < result[0].messages.length; i++) {
        con.query(
          `SELECT * FROM chatmessages WHERE id LIKE '${result[0].messages[i]}'`,
          (err, messageRes) => {
            if (messageRes) {
              result[0].messages[i] = messageRes[0];
            }
          },
        );
      }
      socket.join(`chat${result[0].id}`);

      socket.emit('initChatMessages', result[0].messages);
    } else {
      con.query(
        `INSERT INTO chatgroups (users, messages, createdAt, updatedAt) VALUES ('[${user.id}]', '[]', NOW(), NOW())`,
        (err, res) => {
          console.log(err);
          socket.emit('initChatMessages', []);
          socket.join(`chat${res.insertId}`);
        },
      );
    }
  });
};

const sendChat = (
  con,
  user,
  chatGroup,
  socket,
  chatMessage,
  io,
) => {
  const createdAt = new Date();
  con.query(
    `INSERT INTO chatmessages (user, value, createdAt, updateAt) VALUES ('${user.id}', '${
      JSON.parse(chatMessage).value
    }', NOW(), NOW())`,
    (err, res) => {
      console.log(res)
      con.query(
        `SELECT * FROM chatgroups WHERE id LIKE '${chatGroup}'`,
        (_error, _res) => {
          if (_res[0]) {
            console.log(_res[0]);
            _res[0].messages = JSON.parse(_res[0].messages);
            _res[0].messages.push(res.insertId);
            con.query(
              `UPDATE chatgroups SET messages = '${JSON.stringify(
                _res[0].messages,
              )}' WHERE id LIKE '${chatGroup}'`,
              (error, result) => {
                if (err) console.log(err);
                else {
                  // const socketsInRoom = io.sockets.adapter.rooms.get(`chat${chatRoomId}`));
                  const sockets = Array.from(
                    io.in(`chat${chatGroup}`).adapter.nsp.sockets,
                    ([name, value]) => ({ name, value }),
                  );
                  const users = JSON.parse(_res[0].users).filter(
                    (el) => el != user.id,
                  );
                  for (let i = 0; i < users.length; i++) {
                    const socket = sockets.find(
                      (el) => el.value.userId == users[i],
                    );
                    console.log(socket);
                    if (socket) {
                      socket.value.emit('recieveChatMessage', {
                        id: res.insertId,
                        user: user.id,
                        value: JSON.parse(chatMessage).value,
                        createdAt: createdAt,
                        updatedAt: createdAt,
                      });
                    }else{
                      con.query(`INSERT INTO notifications (value, user, type, createdAt) VALUES ('${JSON.parse(chatMessage).value}', '${chatGroup}', '${users[i]}', 'vrc_chat', NOW())`, (err, resOfInsert)=>{
                        if(err) console.log(err)
                        console.log('added notify')
                      })
                    }
                  }
                }
              },
            );
          }
        },
      );
    },
  );
};

module.exports = {
  initChat,
  sendChat,
};

//token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImEzNGVmZkBhLmNvbSIsInJvbGUiOiJpbnN0cnVjdG9yIiwic2VjcmV0Q29kZSI6IlUyRnNkR1ZrWDEvZkJubzJtVjZwRnJOWUwvSWdwbkRlZmFjODVEeWhLVUVJYjdpaVFhM0FIQzZGT25yejdVZ2siLCJpYXQiOjE2NjQ1MjgwNDN9.C_U_nNUZPKFWXnUmCUEKswbiBK2Gcl0Uu_wT33d9s7M

// var socket_connect = function (room) {
//     return io('localhost:3000', {
//         query: 'sessionType='+room+"&userChatMembers=[7,11]&chatRoomId=1&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImEyQGEuY29tIiwicm9sZSI6InN0dWRlbnQiLCJzZWNyZXRDb2RlIjoiVTJGc2RHVmtYMS9KQVFFWGtVcVNIYTlZUTFyZ28wTUNUUXh4dmJIV1A2TTVlNXI1OC93aW4vUHc1dkliYVlvZSIsImlhdCI6MTY2MTM4NzgwM30.0UE9dItAa7cZasXmEjopKNnh2LclXIDQYYYTFjl7y7Y"
//     });
// }

// var socket      = socket_connect("chat");

// socket.on('initChatMessages', res => {
//     console.log(res)
// })

// socket.emit('sendChatMessage', JSON.stringify({
//     value: 'testing'
// }))

// socket.on('recieveChatMessage', res =>{
//     console.log(res)
// })
