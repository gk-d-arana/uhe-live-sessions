const util = require('util');
// node native promisify

const handleNotificationInit = async (con, socket, user) => {
  const query = util.promisify(con.query).bind(con);
  const res = [];
  (async () => {
    try {
      const rows = await query(
        `SELECT * FROM notifications  WHERE user LIKE '${user.id}' AND type LIKE 'chat' AND isRead LIKE 0`,
      );
      if (rows.length > 0) {
        for (let i = 0; i < rows.length; i++) {
          switch (rows[i].type) {
            case 'chat':
              const chatGroup = await query(
                `SELECT * FROM chatgroups WHERE id LIKE '${rows[i].author}';`,
              );
              if (chatGroup[0]) {
                rows[i].author = {
                  id: chatGroup[0].id,
                  name: chatGroup[0].name,
                };
                res.push(rows[i]);
              }
              break;
            default:
              break;
          }
        }
      }
    } finally {
      socket.emit('initNotifications', res);
    }
  })();
};

const markNotificationAsRead = async (notifyId, res, con, userId) => {
  const query = util.promisify(con.query).bind(con);

  try {
    const result = await query(
      `UPDATE notifications SET isRead = ${true} WHERE id LIKE '${notifyId}' AND user LIKE '${userId}'`,
    );
    if (result.affectedRows > 0) {
      res
        .send(
          JSON.stringify({
            message: 'success',
          }),
        )
        .status('403');
    } else {
      res.status(404).send(
        JSON.stringify({
          message: 'not found',
        }),
      );
    }
  } catch (err) {
    res.status(404).send(
      JSON.stringify({
        message: 'not found',
      }),
    );
  }
};

module.exports = {
  handleNotificationInit,
  markNotificationAsRead,
};
